DROP SCHEMA IF EXISTS adoroF;

CREATE SCHEMA adoroF;

USE adoroF;

CREATE TABLE usuarios(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
nome VARCHAR(60) NOT NULL,
email VARCHAR(60) UNIQUE NOT NULL,
senha VARCHAR(60) NOT NULL
);

CREATE TABLE sinopses(
id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
filme VARCHAR(99) NOT NULL,
classificacao VARCHAR(20) NOT NULL,
sinopse VARCHAR(255) NOT NULL
);
