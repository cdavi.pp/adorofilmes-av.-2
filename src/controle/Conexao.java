package controle;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public final class Conexao {
	public Connection abrirConexao() {
		Connection connect = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String banco = "cms02";
			String servidor= "jdbc:mysql://localhost/" + banco;
			String user = "root";
			String pwd = "";
			connect = DriverManager.getConnection(servidor,user,pwd);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return connect;
	}
	public boolean fecharConexao(Connection con) {
		boolean resultado = false;
		try {
			con.close();
			resultado = true;
		}catch(SQLException e) {
			System.out.println("N�o foi poss�vel fechar a conex�o: " + e.getMessage());
		}
		return resultado;
	}	
}


