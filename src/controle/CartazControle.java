package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelo.Cartaz;
public class CartazControle {
	public boolean adicionarCartaz(Cartaz car){
        boolean resultado = false;
        try{
            Connection con = new Conexao().abrirConexao();
            String sql = "INSERT INTO Cartazes(filme, imagem) VALUES(?,?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, car.getFilme());
            ps.setString(2, car.getImagem());
            if(!ps.execute()) {
                resultado = true;
            }
            new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public boolean alterarCartaz(Cartaz car){
        boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE Cartazes SET filme=?, imagem=? WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, car.getFilme());
            ps.setString(2, car.getImagem());
            ps.setInt(3, car.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public ArrayList<Cartaz> mostrarTodasCartazes(){
    	ArrayList<Cartaz> lista = new ArrayList<Cartaz>();
    	try {
    		Connection con = new Conexao().abrirConexao();
    		PreparedStatement ps = con.prepareStatement("SELECT * FROM Cartazes;");
    		ResultSet rs = ps.executeQuery();
    		if(rs != null) {
    			while(rs.next()) {
    				Cartaz Cartaz = new Cartaz();
    				Cartaz.setId(rs.getInt("id"));
    				Cartaz.setFilme(rs.getString("filme"));
    				Cartaz.setImagem(rs.getString("classificacao"));
    				lista.add(Cartaz);
    			}
    		}
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
    	return lista;
    }
	public boolean deletarCartaz(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM Cartazes WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
}

