package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelo.Trailer;
public class TrailerControle {
	public boolean adicionarTrailer(Trailer tra){
        boolean resultado = false;
        try{
            Connection con = new Conexao().abrirConexao();
            String sql = "INSERT INTO trailers(filme, video) VALUES(?,?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, tra.getFilme());
            ps.setString(2, tra.getVideo());
            if(!ps.execute()) {
                resultado = true;
            }
            new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public boolean alterarTrailer(Trailer tra){
        boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE trailers SET filme=?, video=? WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, tra.getFilme());
            ps.setString(2, tra.getVideo());
            ps.setInt(3, tra.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public ArrayList<Trailer> mostrarTodasTrailers(){
    	ArrayList<Trailer> lista = new ArrayList<Trailer>();
    	try {
    		Connection con = new Conexao().abrirConexao();
    		PreparedStatement ps = con.prepareStatement("SELECT * FROM trailers;");
    		ResultSet rs = ps.executeQuery();
    		if(rs != null) {
    			while(rs.next()) {
    				Trailer trailer = new Trailer();
    				trailer.setId(rs.getInt("id"));
    				trailer.setFilme(rs.getString("filme"));
    				trailer.setVideo(rs.getString("classificacao"));
    				lista.add(trailer);
    			}
    		}
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
    	return lista;
    }
	public boolean deletarTrailer(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM trailers WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
}
