package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.Sinopse;
public class SinopseControle {
	public boolean adicionarSinopse(Sinopse sin){
        boolean resultado = false;
        try{
            Connection con = new Conexao().abrirConexao();
            String sql = "INSERT INTO sinopses(filme, classificacao, sinopse, imagem) VALUES(?,?,?,?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, sin.getFilme());
            ps.setString(2, sin.getClassificacao());
            ps.setString(3, sin.getSinopse());
            ps.setString(4, sin.getImagem());
            if(!ps.execute()) {
                resultado = true;
            }
            new Conexao().fecharConexao(con);
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public boolean alterarSinopse(Sinopse sin){
        boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "UPDATE sinopses SET filme=?, classificacao=?, sinopse=?, imagem=? WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, sin.getId());
			ps.setString(1, sin.getFilme());
            ps.setString(2, sin.getClassificacao());
            ps.setString(3, sin.getSinopse());
            ps.setString(4, sin.getImagem());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
    }
	public ArrayList<Sinopse> mostrarTodasSinopses(){
    	ArrayList<Sinopse> lista = new ArrayList<Sinopse>();
    	try {
    		Connection con = new Conexao().abrirConexao();
    		PreparedStatement ps = con.prepareStatement("SELECT * FROM sinopses;");
    		ResultSet rs = ps.executeQuery();
    		if(rs != null) {
    			while(rs.next()) {
    				Sinopse sinopse = new Sinopse();
    				sinopse.setId(rs.getInt("id"));
    				sinopse.setFilme(rs.getString("filme"));
    				sinopse.setClassificacao(rs.getString("classificacao"));
    				sinopse.setSinopse(rs.getString("sinopse"));
    				sinopse.setImagem(rs.getString("imagem"));
    				lista.add(sinopse);
    			}
    		}
		}catch(SQLException e){
			System.out.println(e.getMessage());
		}
    	return lista;
    }
	public boolean deletarSinopse(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE FROM sinopses WHERE id=?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}

}
