package beans;
import javax.faces.bean.ManagedBean;
import controle.UsuarioControle;
import modelo.Usuario;
import java.util.ArrayList;
// Dando nome ao javaBean
@ManagedBean(name="UserBean")

public class UserBean {
	int id;
	String nome, email, senha;
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String add() {
		Usuario user = new Usuario(this.getId(),this.getNome(),this.getEmail(), this.getSenha());
		new UsuarioControle().add(user);
		return "index";
	}
	
	ArrayList<Usuario> lista = new UsuarioControle().consultarTodos();
	public ArrayList<Usuario> getLista() {
		return lista;
	}
	public void setLista(ArrayList<Usuario> lista) {
		this.lista = lista;
	}
	public Usuario retornarItem(int id) {
		return lista.get(id);
	}
	
	public String deletar(int id) {
		new UsuarioControle().remover(id);
		return "index";
	}
	
	public String editar() {
		Usuario user = new Usuario(this.getId(),this.getNome(), this.getEmail(),this.getSenha());
		if(new UsuarioControle().editar(user)) {
			return "index";
		}else {
			return "index";
		}
	}
	
	public void carregarId(int id) {
		Usuario user = new UsuarioControle().consultaUm(id);
		this.setId(user.getId());
		this.setNome(user.getNome());
		this.setEmail(user.getEmail());
		this.setSenha(user.getSenha());
	}
}